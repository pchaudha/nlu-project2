
import matplotlib.pyplot as plt
import matplotlib.path as mpath
import numpy as np

#Epoch 1 experiment
x_val = np.array([63.85, 65.45, 68.82, 71.07, 72.08, 73.37, 71.23, 72.83, 72.62, 75.13, 74.54, 73.37, 75.35, 75.03, 75.19, 74.92, 75.24])
#Epoch 2 experiment
#x_val = np.array([66.74, 69.09, 70.64, 72.46, 73.31, 71.92, 68.77, 72.83, 71.66, 72.62, 72.51, 72.19, 72.51, 74.01, 74.65, 74.22, 74.22, 73.21, 74.28, 74.12, 74.97, 74.76, 74.01, 75.19, 75.99, 75.72, 74.65, 74.44, 74.81, 74.70, 74.22, 75.13, 75.56, 75.45])
#x_val_2 = np.array([73.21, 74.28, 74.12, 74.97, 74.76, 74.01, 75.19, 75.99, 75.72, 74.65, 74.44, 74.81, 74.70, 74.22, 75.13, 75.56, 75.45])
y = np.array([1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,11000,12000,13000,14000,15000,16000,16474])#, 17000, 18000, 19000, 20000,21000,22000,23000,24000,25000,26000,27000,28000,29000,30000,31000,32000,32948])

#Epoch 1 experiment
x_tr = np.array([63.37, 67.11, 70.0, 72.67, 72.99, 75.72, 79.09, 80.70, 82.62, 83.64, 85.45, 86.68, 88.40, 89.79, 90.59, 91.39, 91.39])
#Epoch 2 experiment
#x_tr = np.array([64.38, 66.52, 69.62, 72.94, 75.29, 76.04, 78.77, 79.04, 81.92, 82.94, 85.08, 85.45, 86.63, 89.84, 90.0, 91.60, 91.98, 91.82, 93.90, 94.22, 94.76, 94.70, 95.78, 95.99, 96.26, 97.11, 97.27, 97.91, 98.07, 98.18, 98.61, 98.82, 99.14, 99.20 ])
#x_tr_2 = np.array([91.82, 93.90, 94.22, 94.76, 94.70, 95.78, 95.99, 96.26, 97.11, 97.27, 97.91, 98.07, 98.18, 98.61, 98.82, 99.14, 99.20 ])

star = mpath.Path.unit_regular_star(6)
circle = mpath.Path.unit_circle()
# concatenate the circle with an internal cutout of the star
verts = np.concatenate([circle.vertices, star.vertices[::-1, ...]])
codes = np.concatenate([circle.codes, star.codes])
cut_star = mpath.Path(verts, codes)
plt.xlim([0, 17000])
plt.ylim([0, 100])
plt.plot(y,x_val, '--g', marker="X", markersize=15, label = "validation")
plt.plot(y,x_tr, '--r', marker="X", markersize=15, label = "train")
plt.grid(True, lw = 2, ls = '--')
# naming the x axis
plt.xlabel('n_update')
# naming the y axis
plt.ylabel('Accuracy(%)')

# giving a title to my graph
plt.title('Training and Validation plot for 2 epoch experiment')
# show a legend on the plot
plt.legend()

plt.show()

print("Done")

