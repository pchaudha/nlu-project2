

import glob, os
import json
import csv
import pandas as pd


import os

#change 2 -> 1
#and 1 -> 0
#from train_race.csv

data = pd.read_csv('/scratch2/NLU/Project2/train_race_full.csv')
data['correct_option'] = data['correct_option'].map({1:0,2:1})
x = data.isnull().values.any()
y = data.isnull().sum().sum()
data.to_csv('/scratch2/NLU/Project2/train_race_full.csv')

print("check")


def split(filehandler, delimiter=',', row_limit=100,
          output_name_template='output_%s.csv', output_path='.', keep_headers=True):
    import csv
    reader = csv.reader(filehandler, delimiter=delimiter)
    current_piece = 1
    current_out_path = os.path.join(
        output_path,
        output_name_template % current_piece
    )
    current_out_writer = csv.writer(open(current_out_path, 'w'), delimiter=delimiter)
    current_limit = row_limit
    if keep_headers:
        headers = next(reader)
        current_out_writer.writerow(headers)
    for i, row in enumerate(reader):
        if i + 1 > current_limit:
            current_piece += 1
            current_limit = row_limit * current_piece
            current_out_path = os.path.join(
                output_path,
                output_name_template % current_piece
            )
            current_out_writer = csv.writer(open(current_out_path, 'w'), delimiter=delimiter)
            if keep_headers:
                current_out_writer.writerow(headers)
        current_out_writer.writerow(row)

split(open('/scratch2/NLU/Project2/train_race_2.csv', 'r'))




data = pd.read_csv("/scratch2/NLU/Project2/train_race.csv",nrows=10000)
data.to_csv('race_frac20.csv')

ds = data.sample(frac=0.2)

ds.to_csv('race_frac20.csv')
print("Done")