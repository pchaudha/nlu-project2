import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix, accuracy_score, precision_recall_fscore_support
import itertools
import seaborn as sns

result = pd.read_csv('/scratch2/NLU/Project2/finetune-transformer-lm-master/submission/gpu_4_iter_2/ROCStories.tsv', sep='\t')

prediction = result['prediction']

ground_truth = pd.read_csv('/scratch2/NLU/Project2/cloze_test_test__spring2016 - cloze_test_ALL_test.csv')

gt = ground_truth['AnswerRightEnding'].map({1:0,2:1})

cf = confusion_matrix(gt, prediction)

#############################################################################################


# Creates a confusion matrix
cm = confusion_matrix(gt, prediction)

# Transform to df for easier plotting
cm_df = pd.DataFrame(cm,
                     index = ['0','1'],
                     columns = ['0','1'])

#plt.figure(figsize=(5.5,4))
sns.heatmap(cm_df, annot=True)
plt.title('Confusion Matrix for One epoch experiment \nAccuracy:{0:.3f}'.format(accuracy_score(gt, prediction)))
plt.ylabel('True label')
plt.xlabel('Predicted label')
plt.show()

###################################################################################################

pd.crosstab(gt, prediction, rownames=['Actual'], colnames=['Predicted'],margins=True)


plt.imshow(cf,cmap=plt.cm.Blues,interpolation='nearest')
plt.colorbar()
plt.title('Confusion Matrix for One epoch experiment \nAccuracy:{0:.3f}'.format(accuracy_score(gt, prediction)))
plt.xlabel('Predicted')
plt.ylabel('Actual')
tick_marks = np.arange(len(set(gt))) # length of classes
class_labels = ['0','1']
tick_marks
plt.xticks(tick_marks,class_labels)
plt.yticks(tick_marks,class_labels)
# plotting text value inside cells
thresh = cf.max() / 2.
for i,j in itertools.product(range(cf.shape[0]),range(cf.shape[1])):
    plt.text(j,i,format(cf[i,j],'d'),horizontalalignment='center',color='white' if cf[i,j] >thresh else 'black')
plt.show();
print("Done")