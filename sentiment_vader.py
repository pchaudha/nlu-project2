
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix, accuracy_score, precision_recall_fscore_support
import itertools
import seaborn as sns

analyser = SentimentIntensityAnalyzer()

# To read a CSV file
train = pd.read_csv('train_stories.csv')
print("Read train_stories")

val = pd.read_csv('cloze_test_test__spring2016 - cloze_test_ALL_test.csv')
print("Read val stories")

df = pd.DataFrame()
df_val = pd.DataFrame()

df_val['polarity_s1'] = val.apply(lambda x: analyser.polarity_scores(x['InputSentence1'])['compound'], axis=1)

df_val['polarity_s2'] = val.apply(lambda x: analyser.polarity_scores(x['InputSentence2'])['compound'], axis=1)
df_val['polarity_s3'] = val.apply(lambda x: analyser.polarity_scores(x['InputSentence3'])['compound'], axis=1)
df_val['polarity_s4'] = val.apply(lambda x: analyser.polarity_scores(x['InputSentence4'])['compound'], axis=1)
df_val['polarity_op1'] = val.apply(lambda x: analyser.polarity_scores(x['RandomFifthSentenceQuiz1'])['compound'], axis=1)
df_val['polarity_op2'] = val.apply(lambda x: analyser.polarity_scores(x['RandomFifthSentenceQuiz2'])['compound'], axis=1)



#Add column right answer to result file
df_val['CorrectAnswer'] = pd.Series(val['AnswerRightEnding'])
df_val.to_csv('result_vader_test.csv')

###-------------------------------------------------------------------------------

#######################################################################

#read the validation result file
res = pd.read_csv('result_vader_test.csv')

sol = pd.DataFrame()
sol['CorrectAnswer'] = pd.Series(res['CorrectAnswer'])
sol['polarity_s4'] = pd.Series(res['polarity_s4'])
sol['polarity_op1'] = pd.Series(res['polarity_op1'])
sol['polarity_op2'] = pd.Series(res['polarity_op2'])
sol['pol_tot'] = pd.Series(res['polarity_s1']+ res['polarity_s2']+ res['polarity_s3'] + res['polarity_s4'])
sol['prediction'] = -1

for index, row in sol.iterrows():
    if row['polarity_s4'] > 0:
        if row['polarity_op1'] > row['polarity_op2']:
            #row['prediction'] = 0
            sol.at[index,'prediction'] = 1
        if row['polarity_op2'] > row['polarity_op1']:
            sol.at[index, 'prediction'] = 2
    if row['polarity_s4'] < 0:
        if row['polarity_op1'] < row['polarity_op2']:
            sol.at[index, 'prediction'] = 1
        if row['polarity_op2'] < row['polarity_op1']:
            sol.at[index, 'prediction'] = 2
    if row['polarity_s4'] == 0:
        if row['pol_tot'] > 0:
            if row['polarity_op1'] > row['polarity_op2']:
                # row['prediction'] = 0
                sol.at[index, 'prediction'] = 1
            if row['polarity_op2'] > row['polarity_op1']:
                sol.at[index, 'prediction'] = 2
        if row['pol_tot'] < 0:
            if row['polarity_op1'] < row['polarity_op2']:
                sol.at[index, 'prediction'] = 1
            if row['polarity_op2'] < row['polarity_op1']:
                sol.at[index, 'prediction'] = 2
        if row['pol_tot'] == 0:
            sol.at[index, 'prediction'] = 1

for index, row in sol.iterrows():
    if row['prediction'] == -1:
        sol.at[index, 'prediction'] = 1



num_matches = 0
total_items = len(sol['prediction'])
for index, row in sol.iterrows():
    if row['CorrectAnswer'] == row['prediction']:
        num_matches = num_matches + 1
print(num_matches/total_items)

cf = confusion_matrix(sol['CorrectAnswer'], sol['prediction'])

plt.imshow(cf,cmap=plt.cm.Blues,interpolation='nearest')
plt.colorbar()
plt.title('Confusion Matrix for Sentiment Analysis \nAccuracy:{0:.3f}'.format(accuracy_score(sol['CorrectAnswer'], sol['prediction'])))
plt.xlabel('Predicted')
plt.ylabel('Actual')
tick_marks = np.arange(len(set(sol['CorrectAnswer']))) # length of classes
class_labels = ['0','1']
tick_marks
plt.xticks(tick_marks,class_labels)
plt.yticks(tick_marks,class_labels)
# plotting text value inside cells
thresh = cf.max() / 2.
for i,j in itertools.product(range(cf.shape[0]),range(cf.shape[1])):
    plt.text(j,i,format(cf[i,j],'d'),horizontalalignment='center',color='white' if cf[i,j] >thresh else 'black')
plt.show();
print("Done")
print("Done")