#BSUB -W 24:00
#BSUB -o /cluster/work/igp_psr/pchaudha/flood/output/class.%J.txt
#BSUB -e /cluster/work/igp_psr/pchaudha/flood/output/class_error.%J.txt
#BSUB -n 1
#BSUB -R "rusage[mem=32768,ngpus_excl_p=1]"
#### BEGIN #####
module load python_gpu/3.7.1
module load hdf5/1.10.1
module load eth_proxy

pip install --user tqdm
pip install --user joblib
pip install --user tqdm
pip install --user ftfy
pip install --user spacy
python3 -m spacy download --user en
python3 train_check.py --dataset rocstories --desc rocstories --submit --analysis --data_dir /cluster/work/igp_psr/pchaudha/flood/nlu/Project2 --log_dir 'log/120h/'
#### END #####
