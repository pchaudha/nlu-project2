# Project Title

NLU 2019: Project 2
Story Cloze Task

## Getting Started

For the training of RACE dataset, we used the code from https://github.com/openai/finetune-transformer-lm as our base or skeleton

## Prerequisites

Following packages should be installed:

```
tqdm
joblib
ftfy
spacy
numpy
csv
os
json
tensorflow
pandas
textblob
Vader
seaborn
```

## Installing

Example to install

```
pip install --user tqdm
pip install --user joblib
pip install --user tqdm
pip install --user ftfy
pip install --user spacy
python3 -m spacy download --user en
pip install -U textblob
python3 -m textblob.download_corpora
pip install --user vaderSentiment
```
## To start training
**Folder: Project2_submission/finetune-transformer-lm-master/**

To start the training use command like mentioned below and just replace path of data_dir and log_dir:

```
python3 train.py --dataset rocstories --desc rocstories --submit --analysis --data_dir /cluster/work/igp_psr/pchaudha/flood/nlu/Project2 --log_dir 'log/120h/'
```
The "--encoder_path" and "--bpe_path" are by default set to model/encoder_bpe_40000.json and model/vocab_40000.bpe. If the model files are not there please set the path by either:
1. Setting at lines 352 and 353 in train.py file or
2. Passing the path as argument for "--encoder_path" and "--bpe_path".

The submission file or result of model on the test file is in finetune-transformer-lm-master/submission/ folder.
The log for training and validation cost and accuracy is in finetune-transformer-lm-master/log folder

## Running on Leonhard cluster

**Folder: Project2_submission/finetune-transformer-lm-master/**

To run the file on Leonhard cluster:
Please use the file train_check_4.py and train_check_4.bash. Also, change the above mentioned flags to proper path in the bash file before running. The ```n_iter``` variable needs to also set before running depending on the number of epoch you want to train. You can either set it in the file or pass as an argument.
By default this file uses 4 gpus. To change please use `n_gpu` argument.

## Running the sentiment files

**Folder: Project2_submission/**

* Please use `sentiment.py` file for TextBlob sentiment library
* And, use `sentiment_vader.py` for Vader sentiment library.
Please make sure the validation and test files are there in the same folder.

## Generating RACE dataset

**RACE dataset original files: Project2_submission/RACE/**
**Generated RACE dataset file location:  Project2_submission/**
The RACE dataset file is included in the folder. To generate again, use the `race_dataset.py` file. Please change the `path_val` and `path_train` variables in the file. 
This will generate a `train_race.csv` file but with correct answer option with value 1 or 2. Use file `train_race_full.csv` which has coorect answer in form 0 and 1. For training purpose, file `train_race_full.csv` has been used.

## Trained Result and log files
For 1 epoch experiment:
**Result file Project2_submission/finetune-transformer-lm-master/submission/gpu_4_iter_1/**

For 2 epoch experiment:
**Result file Project2_submission/finetune-transformer-lm-master/submission/gpu_4_iter_2/**

For 1 epoch experiment:
**Log file Project2_submission/finetune-transformer-lm-master/log/gpu_4_iter_1/**

For 2 epoch experiment:
**Log file Project2_submission/finetune-transformer-lm-master/log/gpu_4_iter_2/**

## Authors
Priyanka Chaudhary,
Simon Brun

### Comment
Other files which have been used for generating plots and confusion matrix are also included. 




