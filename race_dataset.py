
import glob, os
import json
import csv
import pandas as pd

path_val = "/scratch2/NLU/Project2/RACE/dev/"
path_train = "/scratch2/NLU/Project2/RACE/train/"
train_files = []
val_files = []

train_article = []
train_ques = []
train_optns = []
train_ans = []
train_id = []

os.chdir("RACE/train/")

for file in glob.glob("**/*.txt", recursive=True):
    print(file)
    train_files.append(file)

os.chdir(path_val)
for file in glob.glob("**/*.txt", recursive=True):
    print(file)
    val_files.append(file)

#Extract all file data and put into different lists
#articles in train_article
#questions in train_ques
#options in train_optns
#answers in train_ans

for i in train_files:
    temp = path_train+i
    print(temp)
    #text_file = open(temp,"r")
    #text = text_file.readlines()
    with open(temp,"r") as f:
        text = json.load(f)
    article = text["article"]
    ques = text["questions"]
    optns = text["options"]
    ans = text["answers"]
    id = text["id"]

    train_article.append(article)
    train_ques.append(ques)
    train_optns.append(optns)
    train_ans.append(ans)
    train_id.append(id)

train_ans_new = []
#change the answers options from letters to int
for idx, val in enumerate(train_ans):
    l = len(val)
    val_new = []
    for i in val:
        if i == 'A':
            temp = 0
            val_new.append(temp)
        elif i == 'B':
            temp = 1
            val_new.append(temp)
        elif i == 'C':
            temp = 2
            val_new.append(temp)
        elif i == 'D':
            temp = 3
            val_new.append(temp)
        else:
            print("Not a, b, c or d. Error.")
    train_ans_new.append(val_new)


train = []
train_op1 = []
train_op2 = []
train_y = []
train_file = []
train_ids = []

count = 0
for idx, val in enumerate(train_ques):
    l = len(val)
    print(l)
    ar = train_article[idx]
    op = train_optns[idx]
    an = train_ans_new[idx]
    id = train_id[idx]
    print("main: ",val)
    for i in range(l):
        #article + question
        x = ar+val[i]
        #correct option for this question
        correct_op = an[i]
        #available options for this question
        q_options = op[i]
        #get the correct option text
        correct_choice = q_options[correct_op]
        #remove the correct option text from other options
        if correct_choice in q_options:
            q_options.remove(correct_choice)
        if len(q_options) != 3:
            print("3 options should be left instead of ", len(q_options))

        for j in range(len(q_options)):
            if count %2 == 0:
                y = 1
                train.append(x)
                train_op1.append(correct_choice)
                train_op2.append(q_options[j])
                train_y.append(y)
                train_file.append(id)
                train_ids.append(count)
                count= count + 1
            else:
                y = 2
                train.append(x)
                train_op2.append(correct_choice)
                train_op1.append(q_options[j])
                train_y.append(y)
                train_file.append(id)
                train_ids.append(count)
                count = count + 1

newfilePath = "/scratch2/NLU/Project2/train_race.csv"
pd.DataFrame({'id': train_ids, 'story': train, 'option1':train_op1, 'option2':train_op2,'correct_option':train_y,'filename':train_file}).to_csv(newfilePath, index=False)


#change 2 -> 1
#and 1 -> 0
#from train_race.csv

data = pd.read_csv('/scratch2/NLU/Project2/train_race.csv')
data['correct_option'] = data['correct_option'].map({1:0,2:1})
x = data.isnull().values.any()
y = data.isnull().sum().sum()
data.to_csv('/scratch2/NLU/Project2/train_race_full.csv')

print("check")
print("Done")